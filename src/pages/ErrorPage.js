import logo from './logo.gif'

export default function NotFound() {

    return (
        <>
            <h2>Zuitt Booking</h2>
            <h1>Page Not Found</h1>
            <p>Go back to <a href="/">homepage</a>.</p>
            <img src={logo} />
        </>
    )


}